# Vincent, Jean-Philippe and Olivier Portfolio

***

## Purpose

The idea behind this project is to create a simple Open Source website and to expose our learning process with posts containing useful resources.

## Stack

* Backend: [Node.js](http://nodejs.org/)
* Frontend: Not yet decided.
* CSS based on [Twitter's bootstrap](http://getbootstrap.com/)

## Tools

* Taskrunner: [Gulp](http://gulpjs.com/)
* Backend package manager: [Npm](https://www.npmjs.org/)
* Frontend package manager: [Bower](http://bower.io/)

## Installation

1. Install Nodejs
1. Install npm
1. install Bower
1. cd [projectDir]
1. Clone project
1. cd client/
1. npm install
1. bower install
1. gulp // Run Gulp to generate dist/
1. cd ../server
1. npm install
1. nodejs server.js // Start NodeJs server
1. Go to localhost:3000

## License

MIT




