var config = require('./config.js');

var path = require('path'),
	express = require('express'),
	server = express();

server.use(express.static(path.join(config.server.distFolder)));
server.use(function (req, res) {
    res.sendfile(path.join(config.server.distFolder, 'index.html'));
});

server.listen(config.server.listenPort, function() {
    console.log('Listening on port ' + config.server.listenPort);
});
