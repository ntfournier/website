/*global angular */
(function () {
    'use strict';

    angular.module('app').controller('vincentCtrl', ['$scope', function ($scope) {
        $scope.tools = [
            {
                icon: '/assets/img/vincent/icons/ubuntu.svg',
                name: 'Ubuntu'
            },
            {
                icon: '/assets/img/vincent/icons/git.svg',
                name: 'Git'
            },
            {
                icon: '/assets/img/vincent/icons/intellij.svg',
                name: 'IntelliJ Idea'
            },
            {
                icon: '/assets/img/vincent/icons/chrome.svg',
                name: 'Chrome'
            },
            {
                icon: '/assets/img/vincent/icons/vim.svg',
                name: 'Vim'
            }
        ];
    }]);
}());
