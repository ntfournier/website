/*global angular */
(function () {
    'use strict';
    var app = angular.module('app', ['ngRoute']);

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $routeProvider.when('/', {
            templateUrl: '/views/main.html'
        }).when('/projects', {
            templateUrl: '/views/projects/projects.html',
            controller: 'projectCtrl'
        }).when('/developers', {
            templateUrl: '/views/developers/developers.html',
            controller: 'developersCtrl'
        }).when('/developers/vincent', {
            templateUrl: '/views/developers/vincent.html',
            controller: 'vincentCtrl'
        }).when('/developers/jean-philippe', {
            templateUrl: '/views/developers/jeanPhilippe.html'
        }).when('/about', {
            templateUrl: '/views/about/about.html'
        }).otherwise({
                redirectTo: '/'
        });
    }]);

    angular.module('app').controller('projectCtrl', ['$scope', function ($scope) {
        $scope.projectsType = {
            personal: {
                label: 'label-primary',
                glyphicon: 'glyphicon-user',
                background: 'projectVincentPersonalBackground',
                border: 'projectVincentPersonalBorder'
            },
            internship: {
                label: 'label-success',
                glyphicon: 'glyphicon-usd',
                background: 'projectVincentInternshipBackground',
                border: 'projectVincentInternshipBorder'
            },
            school: {
                label: 'label-warning',
                glyphicon: 'glyphicon-home',
                background: 'projectVincentSchoolBackground',
                border: 'projectVincentSchoolBorder'
            }
        };

        $scope.projects = [
            {
                type: 'personal',
                title: 'Webressource.com',
                postTitle: 'Full stack developer',
                postDescription: 'Creation from scratch of this website with stack choices.',
                imageSource: '/assets/img/vincent/website.png'
            },
            {
                type: 'personal',
                title: 'Rowena Notes',
                postTitle: 'Web app developer',
                postDescription: 'Creation of live editor for Jade templating language.',
                imageSource: '/assets/img/vincent/website.png'
            },
            {
                type: 'internship',
                title: 'Robotmaster website',
                postTitle: 'Web developer',
                postDescription: 'Conversion of a PHP website to an AngularJs one running on NodeJs.',
                imageSource: '/assets/img/vincent/robotmaster.png'
            },
            {
                type: 'internship',
                title: 'University of Lyon',
                postTitle: 'Analyst programmer',
                postDescription: 'Creation of a Talend Open Studio task to convert data from an Excel files to the university database.',
                imageSource: '/assets/img/vincent/robotmaster.png'
            },
            {
                type: 'personal',
                title: 'Protect the Sandwich',
                postTitle: 'Game developer, designer and artist',
                postDescription: 'Creation of a small Android game mainly consisting of touching screen to kill wave of insects trying to eat and activating power to help you.',
                imageSource: '/assets/img/vincent/robotmaster.png'
            },
            {
                type: 'internship',
                title: 'Seniors\' center',
                postTitle: 'Analyst programmer',
                postDescription: 'This application was use to manage resident informations, rent payment and waiting list for a residence of 230 appartments.',
                imageSource: '/assets/img/vincent/robotmaster.png'
            },
            {
                type: 'school',
                title: 'Geometry Wars clone',
                postTitle: 'Game developer',
                postDescription: 'A clone made in XNA of the popular Xbox Live Arcade game.',
                imageSource: '/assets/img/vincent/robotmaster.png'
            }
        ];
    }]);
}());
