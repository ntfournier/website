/*global require */

var gulp = require('gulp');

var jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
	usemin = require('gulp-usemin');

// Copy fonts for Bootstrap.
gulp.task('prepareDist', function () {
	gulp.src('bower_components/bootstrap/dist/fonts/*')
	  .pipe(gulp.dest('dist/fonts'));
});

gulp.task('copyAssets', function() {
	gulp.src('app/assets/**/*')
	  .pipe(gulp.dest('dist/assets/'));
});

gulp.task('copyViews', function() {
	gulp.src('app/views/**/*.html')
	  .pipe(gulp.dest('dist/views/'));
});

gulp.task('sass', function () {
    gulp.src('app/scss/*.scss')
        .pipe(sass())
        .pipe(rename('compile.css'))
        .pipe(gulp.dest('app/css'));
});

gulp.task('usemin', function() {
	gulp.src('app/*.html')
	  .pipe(usemin({
	  	 jsLib: []
	  }))
	  .pipe(gulp.dest('dist/'));
});

gulp.task('lint', function() {
    return gulp.src('app/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('watch', function() {
	gulp.watch('app/css/*.css', ['usemin']);
	gulp.watch('app/scss/*.scss', ['sass']);
    gulp.watch('app/js/**/*.js', ['lint', 'usemin']);
    gulp.watch('app/*.html', ['usemin']);
    gulp.watch('app/views/**/*.html', ['copyViews']);
});

// Default Task
gulp.task('default', ['sass', 'lint', 'prepareDist', 'copyAssets', 'usemin', 'copyViews', 'watch']);
